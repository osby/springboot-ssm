package org.yong.springbootssm.model.enums;

public enum Gender {
    MALE,
    FEMALE
}
