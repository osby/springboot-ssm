package org.yong.springbootssm.ws.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.yong.springbootssm.ws.util.WebSocketUtil;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

@Component
@ServerEndpoint("/ws/{user}")
public class WsIndexController {

    private static final Logger LOGGER = LoggerFactory.getLogger(WsIndexController.class);

    private Session session;

    @OnOpen
    public void onOpen(Session session, @PathParam("user") String user) {
        this.session = session;
        WebSocketUtil.welcome(session);
        System.out.println("有一个链接被打开: " + user);
    }

    @OnMessage
    public void onMessage(String msg, Session session) {
        WebSocketUtil.broadcast(msg, session);
    }

    @OnClose
    public void onClose() {
        WebSocketUtil.remove(this.session);
    }

    @OnError
    public void onError(Throwable throwable, Session session) {
        LOGGER.warn("发生错误", throwable);
    }

}
