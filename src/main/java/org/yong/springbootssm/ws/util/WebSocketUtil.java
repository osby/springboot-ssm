package org.yong.springbootssm.ws.util;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.java2d.opengl.WGLSurfaceData;

import javax.websocket.RemoteEndpoint;
import javax.websocket.Session;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class WebSocketUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketUtil.class);
    private static final AtomicInteger ONLINE_COUNT = new AtomicInteger(0);
    private static final ConcurrentHashMap<String, Session> SESSION_MAP = new ConcurrentHashMap<>();

    public static void welcome(Session session) {
        try {
            sendMsg(session, getWelcomeMsg());
            ONLINE_COUNT.addAndGet(1);
            SESSION_MAP.put(session.getId(), session);
        } catch (Exception e) {
            LOGGER.warn("发送消息时发生错误: " + e.getMessage(), e);
        }
    }

    public static void broadcast(String msg, Session currentSession) {
        for (Map.Entry<String, Session> me : SESSION_MAP.entrySet()) {
            Session session = me.getValue();
            try {
                sendMsg(session, msg);
            } catch (Exception e) {
                LOGGER.warn("发送消息时发生错误: " + e.getMessage(), e);
            }
        }

    }

    public static void remove(Session session) {
        SESSION_MAP.remove(session.getId());
        ONLINE_COUNT.set(SESSION_MAP.size());
    }

    private static void sendMsg(Session session, String msg) throws Exception {
        RemoteEndpoint.Basic basic = session.getBasicRemote();
        String dataJson = convertJSON(msg);
        basic.sendText(dataJson);
    }

    private static String convertJSON(String msg) {
        Map<String, Object> data = Maps.newHashMap();
        data.put("data", msg);
        data.put("time", new Date());
        return JSON.toJSONString(data);
    }

    private static String getWelcomeMsg() {
        return "欢迎进入~, 当前人数:" + ONLINE_COUNT.get();
    }

}
