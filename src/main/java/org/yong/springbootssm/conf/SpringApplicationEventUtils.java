package org.yong.springbootssm.conf;

import org.springframework.boot.SpringApplication;
import org.springframework.stereotype.Component;

@Component
public class SpringApplicationEventUtils {


    public static void listen(SpringApplication sa, String[] args) {
        sa.addListeners(new ApplicationStartingListener());
        sa.addListeners(new ApplicationPreparedListener());
        sa.addListeners(new ApplicationReadyEventListener());
    }

}
