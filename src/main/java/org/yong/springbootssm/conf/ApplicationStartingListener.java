package org.yong.springbootssm.conf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationStartingEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class ApplicationStartingListener implements ApplicationListener<ApplicationStartingEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationStartingListener.class);

    @Override
    public void onApplicationEvent(ApplicationStartingEvent event) {
        LOGGER.info("Invoke spring-boot starting listener: " + event);
    }

}
