package org.yong.springbootssm.conf.mvc;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebMvcConfigurer extends WebMvcConfigurerAdapter {

    // 静态资源映射
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry
//                // 添加资源处理器
//                .addResourceHandler("/newRes/**")
//                // 在classpath下, /my/文件夹中
//                // .addResourceLocations("classpath:/newRes/");
//                // 也可以指定外部文件夹
//                .addResourceLocations("file:C:\\Pictures\\");

        super.addResourceHandlers(registry);
    }

    // 添加视图映射
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
//        registry
//                // 设置URL路径
//                .addViewController("/toLogin")
//                // 设置视图虚拟路径
//                .setViewName("/login");
        super.addViewControllers(registry);
    }

    // 添加拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
//        registry
//                // 添加拦截器
//                .addInterceptor(new LoginInterceptor())
//                // 拦截规则
//                .addPathPatterns("/**")
//                // 需要排除的路径
//                .excludePathPatterns("/toLogin", "/login");
        super.addInterceptors(registry);
    }

}
