package org.yong.springbootssm.conf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.yong.springbootssm.conf.db.DBInitializer;
import org.yong.springbootssm.conf.db.DbOption;
import org.yong.springbootssm.util.StringUtil;

@Component
public class ApplicationReadyEventListener implements ApplicationListener<ApplicationReadyEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationReadyEventListener.class);

    @Value("${db.option}")
    private String dbOption;

    @Autowired
    private DBInitializer dbInitializer;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationPreparedEvent) {
        if (null != dbInitializer) {
            LOGGER.info("Invoke ApplicationReadyEventListener");
            DbOption dbOption = getDbOption();
            dbInitializer.invoke(dbOption);
        }
    }

    private DbOption getDbOption() {
        String option = StringUtil.defaultString(this.dbOption, DbOption.NONE.name());
        return DbOption.valueOf(option);
    }

}
