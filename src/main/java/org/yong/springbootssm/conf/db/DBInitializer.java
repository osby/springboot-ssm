package org.yong.springbootssm.conf.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
import org.yong.springbootssm.conf.db.mapper.DBInfoMapper;
import org.yong.springbootssm.conf.db.model.DatabaseInfo;
import org.yong.springbootssm.util.DateUtil;
import org.yong.springbootssm.util.db.DBUtil;
import sun.swing.FilePane;

import java.io.File;
import java.io.FileNotFoundException;

@Component
public class DBInitializer {

    private static final Logger LOGGER = LoggerFactory.getLogger(DBInitializer.class);

    @Value("${db.backup.dir}")
    private String backupDir;

    @Value("${db.backup.started}")
    private boolean backupStarted;

    private String dbName;

    @Value("${spring.datasource.url}")
    public void setDbName(String url) {
        int lastSepIndex = url.lastIndexOf("/");
        this.dbName = url.substring(lastSepIndex + 1);
    }

    @Value("${db.init.file}")
    private String dbInitFile;

    @Autowired
    private DBUtil dbUtil;

    @Autowired
    private DBInfoMapper dbInfoMapper;

    public void invoke(DbOption dbOption) {
        LOGGER.info("DBInitializer do something: " + dbOption);
        switch (dbOption) {
            case NONE:
                break;
            case OVERRIDE:
                if (hasAlreadyExistDB())
                    break;
            case INSTALL:
                invokeInitSqlFile();
                break;
        }

        initBackupDb();
    }

    private void initBackupDb() {
        try {
            if (backupStarted) {
                LOGGER.info("waiting for database backup ...");

                File dir = ResourceUtils.getFile(backupDir);
                File sqlFile = new File(dir, getBackupFileName());
                File file = dbUtil.exportMySql(sqlFile);
                LOGGER.info("Backup SQL file[" + file.getAbsolutePath() + "] done");
            } else {
                LOGGER.info("Skip database backup");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String getBackupFileName() {
        return DateUtil.formatNow("yyyyMMdd") + "-" + dbName + ".sql";
    }

    private boolean hasAlreadyExistDB() {
        LOGGER.info("Check database is exist.");
        DatabaseInfo tables = dbInfoMapper.selectDatabaseInfo(this.dbName);
        return tables.hasTables();
    }

    private void invokeInitSqlFile() {
        LOGGER.info("===> invoke initial sql file.");
        dbUtil.execSqlFileByMysql(dbInitFile, DBUtil.SqlSeparator.MYSQL);
        LOGGER.info("===> invoke done");
    }
}
