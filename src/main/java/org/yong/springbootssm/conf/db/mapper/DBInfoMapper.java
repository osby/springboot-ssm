package org.yong.springbootssm.conf.db.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.yong.springbootssm.conf.db.model.DatabaseInfo;

@Repository
public interface DBInfoMapper {

    DatabaseInfo selectDatabaseInfo(@Param("dbName") String dbName);

    String selectInstallDir();

}
