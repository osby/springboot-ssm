package org.yong.springbootssm.conf.db.model;

import java.io.Serializable;
import java.util.List;

public class DatabaseInfo implements Serializable {
    private static final long serialVersionUID = 6144370676866345355L;

    private String dbName;
    private List<TableInfo> tableInfos;

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public List<TableInfo> getTableInfos() {
        return tableInfos;
    }

    public void setTableInfos(List<TableInfo> tableInfos) {
        this.tableInfos = tableInfos;
    }

    @Override
    public String toString() {
        return "DatabaseInfo{" +
                "dbName='" + dbName + '\'' +
                ", tableInfos=" + tableInfos +
                '}';
    }

    public boolean hasTables() {
        return !tableInfos.isEmpty();
    }

}
