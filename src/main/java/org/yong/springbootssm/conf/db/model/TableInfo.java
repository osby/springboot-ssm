package org.yong.springbootssm.conf.db.model;

import java.io.Serializable;
import java.util.Date;

public class TableInfo implements Serializable {

    private static final long serialVersionUID = -6931893793536030029L;

    private String tableName;     // 表名
    private String tableRows;       // 数据行数
    private String tableComment;    // 表注释
    private Date createTime;        // 创建时间
    private Date updateTime;        // 更新时间

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableRows() {
        return tableRows;
    }

    public void setTableRows(String tableRows) {
        this.tableRows = tableRows;
    }

    public String getTableComment() {
        return tableComment;
    }

    public void setTableComment(String tableComment) {
        this.tableComment = tableComment;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "TableInfo{" +
                "tableName='" + tableName + '\'' +
                ", tableRows='" + tableRows + '\'' +
                ", tableComment='" + tableComment + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
