package org.yong.springbootssm.conf.db;

public enum DbOption {
    /**
     * 强制覆盖安装
     */
    OVERRIDE,

    /**
     * 检测安装
     */
    INSTALL,

    /**
     * 无任何操作
     */
    NONE
}