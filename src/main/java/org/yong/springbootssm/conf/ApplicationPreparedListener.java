package org.yong.springbootssm.conf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationPreparedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class ApplicationPreparedListener implements ApplicationListener<ApplicationPreparedEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationPreparedListener.class);

    @Override
    public void onApplicationEvent(ApplicationPreparedEvent event) {
        LOGGER.info("Invoke spring-boot starting listener: " + event);
    }

}
