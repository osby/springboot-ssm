package org.yong.springbootssm.build;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.yong.springbootssm.conf.SpringApplicationEventUtils;

import java.util.Arrays;

@SpringBootApplication
@ComponentScan(basePackages = {"org.yong"})
@ServletComponentScan(basePackages = {"org.yong"})
@MapperScan(basePackages = {"org.yong"})
public class SpringbootSsmApplication {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(SpringbootSsmApplication.class);
        SpringApplicationEventUtils.listen(springApplication, args);
        springApplication.run(args);
    }
}
