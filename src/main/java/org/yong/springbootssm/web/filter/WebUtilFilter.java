package org.yong.springbootssm.web.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;
import org.yong.springbootssm.web.util.WebUtil;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ComponentScan
@WebFilter(filterName = "WebUtilFilter", urlPatterns = "/*")
public class WebUtilFilter implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebUtilFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        WebUtil.setRequest(request);
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        WebUtil.setResponse(response);
        WebUtil.setSession(request.getSession());
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }

}
