package org.yong.springbootssm.web.util;

import com.alibaba.fastjson.JSON;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.ui.ModelMap;
import org.yong.springbootssm.model.entities.User;
import org.yong.springbootssm.util.Constants;
import org.yong.springbootssm.util.StringUtil;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

public class WebUtil {

    private static final ThreadLocal<HttpServletRequest> REQUEST = new ThreadLocal<>();

    private static final ThreadLocal<HttpServletResponse> RESPONSE = new ThreadLocal<>();

    private static final ThreadLocal<HttpSession> SESSION = new ThreadLocal<>();

    public static void setRequest(HttpServletRequest req) {
        REQUEST.set(req);
    }

    public static HttpServletRequest getRequest() {
        return REQUEST.get();
    }

    public static void setResponse(HttpServletResponse resp) {
        RESPONSE.set(resp);
    }

    public static HttpServletResponse getResponse() {
        return RESPONSE.get();
    }

    public static void setSession(HttpSession session) {
        SESSION.set(session);
    }

    public static HttpSession getSession() {
        return SESSION.get();
    }


    public static ModelMap successModelMap(Object data) {
        return getModelMap(true, data, null, false);
    }

    public static ModelMap successModelMap() {
        return getModelMap(true, null, null, false);
    }

    public static ModelMap getModelMap() {
        return getModelMap(true, null, null, false);
    }

    public static ModelMap getModelMap(boolean flag) {
        return getModelMap(flag, null, null, false);
    }

    public static ModelMap getModelMap(Object data) {
        return getModelMap(true, data, null, false);
    }

    public static ModelMap getModelMap(String msg) {
        return getModelMap(false, null, msg, false);
    }

    public static ModelMap getModelMap(boolean flag, String message) {
        return getModelMap(flag, null, message);
    }

    public static ModelMap getModelMap(boolean flag, Object data) {
        return getModelMap(flag, data, null);
    }

    public static ModelMap getModelMap(boolean flag, Object data, String message) {
        return getModelMap(flag, data, message, false);
    }

    public static ModelMap getModelMap(boolean flag, Object data, String message, boolean isJsonp) {
        ModelMap modelMap = new ModelMap("flag", flag).addAttribute("data", data).addAttribute("message", message);
        if (isJsonp) {
            String callback = getParameter("callback");
            if (StringUtils.isNotBlank(callback)) {
                String msg = callback + "(" + JSON.toJSONString(modelMap) + ")";
                WebUtil.writeMsg(msg, getResponse());
                return null;
            }
        }
        return modelMap;
    }

    public static <T> T getSessionAttr(String key) {
        return (T) getSession().getAttribute(key);
    }

    public static void setSessionAttr(String key, Object value) {
        getSession().setAttribute(key, value);
    }

    public static void setRequestAttr(String key, Object value) {
        getRequest().setAttribute(key, value);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getRequestAttr(String key) {
        return (T) getRequest().getAttribute(key);
    }

    public static String getClassPath() {
        URL resource = Thread.currentThread().getContextClassLoader().getResource("");
        String path = resource.getPath();

        if (isWindowsSys())
            path = path.substring(1);

        return path;
    }

    private static boolean isWindowsSys() {
        return "\\".equals(System.getProperty("file.separator"));
    }

    /**
     * 获取项目根路径(文件系统)
     *
     * @return 项目相对路劲, 总是以"/"结尾
     */
    public static String getBaseFilePath() {
        String basePath = getRequest().getSession().getServletContext().getRealPath("/");
        basePath = basePath.replace("\\", "/");
        if (!basePath.endsWith("/"))
            basePath += "/";
        return basePath;
    }

    /**
     * 获取文件, 总是保证文件夹一定存在.
     *
     * @param path 相对项目根径
     * @return 文件对象
     * @throws IOException
     */
    public static File getFile(String path) throws IOException {
        path = StringUtils.isBlank(path) ? StringUtils.EMPTY : path.replace("\\", "/");

        // 获取文件路径
        String baseFilePath = getBaseFilePath();
        String pathName = path;
        File file = null;
        if (pathName.startsWith(baseFilePath)) {
            file = new File(pathName);
        } else {
            file = new File(baseFilePath, pathName);
        }

        // 保证文件存在
        if (!file.exists()) {
            File dir = file.getParentFile();
            if (!dir.exists()) {
                dir.mkdirs();
            }
            dir.createNewFile();
        }
        return file;
    }

    public static String getClientIp() {
        HttpServletRequest request = getRequest();
        String ip = request.getHeader("X-Forwarded-For");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

    public static String getParameter(String paramName) {
        return getRequest().getParameter(paramName);
    }

    public static void download(File downloadableFile) throws Exception {
        download(downloadableFile, null);
    }

    public static void download(File downloadableFile, String displayFileName) throws Exception {
        HttpServletResponse resp = getResponse();
        resp.setContentType("application/download");
        if (StringUtils.isBlank(displayFileName)) {
            displayFileName = downloadableFile.getName();
        }
        String encode = URLEncoder.encode(displayFileName, "UTF-8");
        encode = encode.replace("+", "%20");
        resp.setHeader("content-disposition", "attachment; filename=" + encode);

        ServletOutputStream outputStream = resp.getOutputStream();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(downloadableFile);
            IOUtils.copy(fis, outputStream);
        } catch (Exception e) {
            // ignore
        } finally {
            IOUtils.closeQuietly(fis);
        }
    }

    public static String getBaseUrl() {
        HttpServletRequest request = getRequest();
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName();

        int port = request.getServerPort();
        if (80 != port)
            basePath += ":" + port;
        return basePath + path + "/";
    }

    public static String getFullUrl(String url) {
        String baseUrl = WebUtil.getBaseUrl();
        url = StringUtil.trimToEmpty(url);
        if (StringUtil.isNotBlank(ROOT_CONTEXT))
            url = ROOT_CONTEXT + "/" + url;

        String rUrl = baseUrl + "/" + url;

        return rUrl
                .replaceAll("\\+", "/")
                .replaceAll("/+", "/");
    }


    public static <T> T setParams(Class<T> clazz) {
        try {
            T param = clazz.newInstance();
            BeanUtils.copyProperties(param, getRequest().getParameterMap());
            return param;
        } catch (Exception e) {
            return null;
        }
    }

    public static void writeMsg(String msg, HttpServletResponse resp) {
        Writer out = null;
        try {
            out = resp.getWriter();
            IOUtils.write(msg, out);
        } catch (IOException e) {
            // Ignore
        } finally {
            IOUtils.closeQuietly(out);
        }
    }

    public static void writeMsg(String msg) {
        Writer out = null;
        try {
            out = getResponse().getWriter();
            IOUtils.write(msg, out);
        } catch (IOException e) {
            // Ignore
        } finally {
            IOUtils.closeQuietly(out);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T removeSessionAttr(String sessionKey) {
        HttpSession session = getSession();
        T t = (T) session.getAttribute(sessionKey);
        session.removeAttribute(sessionKey);
        return t;
    }

    public static File getFileDir(String dirPath) throws IOException {
        File dir = getFile(dirPath);
        if (!dir.exists())
            dir.mkdirs();
        return dir;
    }

    public static String getContextUrl() {
        String contextUrl = null;
        String servletPath = getRequest().getServletPath();
        if (StringUtils.isNotBlank(servletPath)) {
            int startIndex = servletPath.indexOf("/");
            int endIndex = servletPath.indexOf("/", startIndex + 1);
            contextUrl = servletPath.substring(startIndex + 1, endIndex);
        }
        return contextUrl;
    }

    public static void writeJSON(Object data) {
        HttpServletResponse resp = getResponse();
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");

        String msg = JSON.toJSONString(data);
        WebUtil.writeMsg(msg, resp);
    }

    public static void setUser(User user) {
        WebUtil.setSessionAttr(Constants.SESSION_USER, user);
    }

    public static <T> T getUser() {
        return WebUtil.getSessionAttr(Constants.SESSION_USER);
    }

    public static <T> void setUser(T user) {
        WebUtil.setSessionAttr(Constants.SESSION_USER, user);
    }

    public static User removeUser() {
        return WebUtil.removeSessionAttr(Constants.SESSION_USER);
    }

    public static void forwardParameters() {
        Map<?, ?> map = getRequest().getParameterMap();
        for (Map.Entry<?, ?> me : map.entrySet())
            setRequestAttr(String.valueOf(me.getKey()), me.getValue());
    }

    /**
     * @see RequestType
     */
    public static RequestType requestType() {
        HttpServletRequest req = getRequest();
        String xReq = req.getHeader("X-Requested-With");

        RequestType r = RequestType.NORMAL;
        if ("XMLHttpRequest".equals(xReq))
            r = RequestType.AJAX;
        return r;
    }

    public static void setCurrentResponseType(ResponseType type) {
        type.invoke();
    }

    private static String ROOT_CONTEXT;

    public static void setRootContext(String rootContext, boolean refresh) {
        if (refresh) {
            ROOT_CONTEXT = rootContext;
            return;
        }

        if (StringUtil.isNotBlank(ROOT_CONTEXT))
            throw new RuntimeException("Has already initialized: ROOT CONTEXT.");
    }

    public enum RequestType {
        /**
         * ajax请求
         * X-Requested-With:XMLHttpRequest
         */
        AJAX,
        /**
         * 文件上传
         */
        FILE_UPLOAD,
        /**
         * 普通请求
         */
        NORMAL
    }

    public enum ResponseType {
        HTML {
            @Override
            public void invoke() {
                HttpServletResponse resp = getResponse();
                resp.setContentType("text/html;charset=utf-8");
            }
        };

        public abstract void invoke();

    }
}
