package org.yong.springbootssm.web.controller.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

@Controller
@RequestMapping("/fileMgr")
public class FileManagerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileManagerController.class);

    @RequestMapping("/upload")
    @ResponseBody
    public ModelMap upload(
            @RequestParam(value = "file", required = false) MultipartFile file,
            HttpServletRequest req) throws Exception {

        ModelMap modelMap = new ModelMap();
        modelMap.put("size", file.getSize());
        modelMap.put("name", file.getName());
        modelMap.putAll(req.getParameterMap());

        File targetFile = getTargetFile();
        file.transferTo(targetFile);
        System.out.println(targetFile.getAbsolutePath());

        return modelMap;
    }

    private File getTargetFile() {
        File classesDir = new File(ClassUtils.getDefaultClassLoader().getResource("").getPath());
        return new File(classesDir, "/123.png");
    }

    @RequestMapping("/img")
    public void getFile(HttpServletResponse resp) throws Exception {
        File file = getTargetFile();
        try (
                FileInputStream in = new FileInputStream(file);
                ByteArrayOutputStream baos = new ByteArrayOutputStream()
        ) {
            int data;
            while ((data = in.read()) != -1)
                baos.write(data);

            OutputStream out = resp.getOutputStream();
            out.write(baos.toByteArray());
            out.flush();
        }
    }
}
