package org.yong.springbootssm.web.controller.common;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping({"", "/"})
public class IndexController {

    @RequestMapping({"", "/"})
    @ResponseBody
    public String index() {
        return "Default index content";
    }

    @RequestMapping("/v/{.+}")
    public String dynamicView(@PathVariable("path") String path) {
        return "redirect:/" + path;
    }

}
