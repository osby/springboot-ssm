/**
 * @FileName: OgnlUtil.java
 * @Author Huang.Yong
 * @Description:
 * @Date 2015年10月15日 下午3:06:27
 * @CopyRight CNP Corporation
 */
package org.yong.springbootssm.util;

import org.apache.ibatis.ognl.Ognl;
import org.apache.ibatis.ognl.OgnlContext;
import org.apache.ibatis.ognl.OgnlException;

/**
 * 对象导航图工具
 *
 * @author Huang.Yong
 */
public abstract class OgnlUtil {

    @SuppressWarnings("unchecked")
    public static <T> T getValue(String ognl, Object object) throws OgnlException {
        OgnlContext context = getOgnlContext(object);
        return (T) Ognl.getValue(createExpression(ognl), context, context.getRoot());
    }

    @SuppressWarnings("unchecked")
    public static <T> T getValue(String ognl, Object object, T defaultValue) throws OgnlException {
        OgnlContext context = getOgnlContext(object);
        try {
            return (T) Ognl.getValue(createExpression(ognl), context, context.getRoot());
        } catch (OgnlException e) {
            // ignore
            return defaultValue;
        }
    }

    /**
     * 设置属性值
     *
     * @param ognl   属性导航图
     * @param value  属性值
     * @param object 目标对象
     */
    public static void setValue(String ognl, Object value, Object object) throws OgnlException {
        OgnlContext context = getOgnlContext(object);
        Ognl.setValue(createExpression(ognl), context, context.getRoot(), value);
    }

    private static OgnlContext getOgnlContext(Object object) {
        OgnlContext context = new OgnlContext();
        context.setRoot(object);
        return context;
    }

    private static Object createExpression(String ognl) throws OgnlException {
        return Ognl.parseExpression(ognl);
    }

}
