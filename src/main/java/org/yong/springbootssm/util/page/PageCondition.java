package org.yong.springbootssm.util.page;

import java.io.Serializable;

public class PageCondition implements Serializable {

    // @Fields serialVersionUID :
    private static final long serialVersionUID = 1890292905429310303L;

    private Integer pageIndex = 1;

    private Integer pageSize = 10;

    private Integer pageNumCount = 5;

    /**
     * 获取当前页码
     *
     * @return Integer 当前页码
     */
    public Integer getPageIndex() {
        return pageIndex;
    }

    /**
     * 设置当前页码
     *
     * @param pageIndex 当前页码
     */
    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    /**
     * 获取分页大小
     *
     * @return Integer 分页大小
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     * 设置分页大小
     *
     * @param pageSize 分页大小
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * 获取页码数量
     *
     * @return Integer 页码数量
     */
    public Integer getPageNumCount() {
        return pageNumCount;
    }

    /**
     * 设置页码数量
     *
     * @param pageNumCount 页码数量
     */
    public void setPageNumCount(Integer pageNumCount) {
        this.pageNumCount = pageNumCount;
    }

    @Override
    public String toString() {
        return "PageCondition [pageIndex=" + pageIndex + ", pageSize=" + pageSize + ", pageNumCount=" + pageNumCount + "]";
    }

}
