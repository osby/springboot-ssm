package org.yong.springbootssm.util.page;

import java.util.List;

/**
 * 分页数据处理器
 *
 * @param <E> 数据类型
 */
public interface DataHandler<E> {

    /**
     * 获取数据列表
     *
     * @param pageSize  页大小
     * @param pageIndex 页码
     * @return List&lt;E&gt; 数据列表
     */
    List<E> getElements(int pageIndex, int pageSize);

    /**
     * 获取数据总行数
     *
     * @return Integer 总行数
     */
    Integer getRowCount();

}
