package org.yong.springbootssm.util;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;
import org.apache.commons.io.FileUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件工具
 *
 * @author yong2
 */
public class FileUtil extends FileUtils {

    /**
     * 文件包装器
     */
    public static class FileWrapper {
        /**
         * 目标文件
         */
        public final File FILE;

        /**
         * 文件路径
         */
        public final String PATH;

        /**
         * 原文件名
         */
        public final String ORIGIN_FILE_NAME;

        /**
         * 文件类型
         */
        public final String FILE_TYPE;

        /**
         * @param file         目标文件
         * @param path         相对路径
         * @param origFileName 原文件名
         * @param fileType     文件类型(后缀)
         */
        private FileWrapper(File file, String path, String origFileName, String fileType) {
            this.FILE = file;
            this.PATH = path;
            this.ORIGIN_FILE_NAME = origFileName;
            this.FILE_TYPE = fileType;
        }

        @Override
        public String toString() {
            return "FileWrapper [FILE=" + FILE + ", PATH=" + PATH + ", ORIGIN_FILE_NAME=" + ORIGIN_FILE_NAME + ", FILE_TYPE="
                    + FILE_TYPE + "]";
        }
    }

    /**
     * 文件上传到Tomcat的webapps内部
     *
     * @param multipartFile SpringMVC文件上传对象
     * @param projectRoot   项目根目录绝对路径
     * @param uploadDir     保存文件夹
     * @param projectInner  是否在项目内部
     * @return 文件包装器
     */
    public static FileWrapper upload(MultipartFile multipartFile, String projectRoot, String uploadDir, boolean projectInner) {
        // 获取文件名
        String fileType = getSuffix(multipartFile.getOriginalFilename());
        String fileName = StringUtil.genGUID() + "." + fileType;

        // 获取文件夹
        if (projectRoot.endsWith("/"))
            projectRoot = projectRoot.substring(0, projectRoot.lastIndexOf('/'));
        if (!projectInner)
            projectRoot = projectRoot.substring(0, projectRoot.lastIndexOf('/'));

        String filePath = uploadDir + "/" + fileName;
        try {
            File dest = new File(projectRoot, filePath);
            if (!dest.exists()) {
                File parentFile = dest.getParentFile();
                if (!parentFile.exists())
                    parentFile.mkdirs();
                dest.createNewFile();
            }
            multipartFile.transferTo(dest);
            return new FileWrapper(dest, filePath, multipartFile.getOriginalFilename(), fileType);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 创建文件所在目录
     *
     * @param fileName 完整文件名
     */
    public static void createFileParentDir(String fileName) {
        File file = new File(fileName);
        File dir = file.getParentFile();
        if (!dir.exists())
            dir.mkdirs();
    }

    /**
     * 列出目录中所有文件
     *
     * @param dir 目录
     * @return 文件映射, Key-文件名, Value-文件
     */
    public static Map<String, File> mapDir(File dir) {
        if (null == dir || dir.isFile())
            return null;
        Map<String, File> map = Maps.newLinkedHashMap();
        File[] files = dir.listFiles();
        if (null == files)
            return map;
        for (File file : files)
            map.put(file.getName(), file);
        return map;
    }

    /**
     * 读取文件内容
     *
     * @param file    文件
     * @param charSet 字符集
     * @return 内容列表
     */
    public static List<String> readLines(File file, String charSet) {
        try {
            return FileUtils.readLines(file, charSet);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取文件名后缀, 不包含点号(.)
     *
     * @param fileName 文件名
     * @return 文件名后缀
     */
    private static String getSuffix(String fileName) {
        int index = fileName.lastIndexOf('.');
        if (-1 == index)
            return StringUtil.EMPTY;
        return fileName.substring(index + 1);
    }
}
