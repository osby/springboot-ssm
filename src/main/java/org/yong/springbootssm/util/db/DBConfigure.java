package org.yong.springbootssm.util.db;

import java.util.List;

/**
 * 数据库配置
 */
public class DBConfigure {

    /**
     * 主机
     */
    public final String HOST;

    /**
     * 数据库用户名
     */
    public final String USERNAME;

    /**
     * 数据库密码
     */
    public final String PASSWORD;

    /**
     * 端口
     */
    public final String PORT;

    /**
     * 库名
     */
    public final String DBNAME;

    /**
     * 表名列表
     */
    public final List<String> TABLES;

    /**
     * 导入或导出文件路径
     */
    public final String FILE_PATH;


    public DBConfigure(String host, String username, String password,
                       String port, String dbName, List<String> tables,
                       String filePath) {
        this.HOST = host;
        this.USERNAME = username;
        this.PASSWORD = password;
        this.PORT = port;
        this.DBNAME = dbName;
        this.TABLES = tables;
        this.FILE_PATH = filePath;
    }

    public DBConfigure(String username, String password, String dbName, List<String> tables, String filePath) {
        this("127.0.0.1", username, password, "3306", dbName, tables, filePath);
    }

    public DBConfigure(String username, String password, String dbName, String filePath) {
        this("127.0.0.1", username, password, "3306", dbName, null, filePath);
    }


    public String getExportCommand() {
        return new StringBuilder()
                .append(" mysqldump ")
                .append(" -u").append(this.USERNAME)
                .append(" -p").append(this.PASSWORD)
                .append(" ").append(this.DBNAME)
                .append(" -r ").append(this.FILE_PATH).toString();
    }

    public String getRestoreCommand() {
        return null;
    }
}