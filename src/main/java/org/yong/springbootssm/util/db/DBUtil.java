package org.yong.springbootssm.util.db;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
import org.yong.springbootssm.util.FileUtil;

import javax.sql.DataSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.Connection;

@Component
public class DBUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(DBUtil.class);

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.password}")
    private String password;

    private String dbName;

    @Value("${spring.datasource.url}")
    public void setDbName(String url) {
        int lastSepIndex = url.lastIndexOf("/");
        this.dbName = url.substring(lastSepIndex + 1);
    }

    @Autowired
    private DataSource dataSource;

    public void execSqlFileByMysql(String filePath, SqlSeparator sqlSeparator) {

        try (
                Connection conn = dataSource.getConnection();
                PrintWriter logWriter = new PrintWriter(System.out)
        ) {

            File file = ResourceUtils.getFile(filePath);
            ScriptRunner runner = new ScriptRunner(conn);

            runner.setAutoCommit(true);//自动提交
            runner.setFullLineDelimiter(false);
            //命令分隔符
            runner.setDelimiter(sqlSeparator.SEPARATOR);
            runner.setSendFullScript(false);
            runner.setStopOnError(true);
            runner.setLogWriter(logWriter);//设置是否输出日志

            //如果又多个sql文件，可以写多个runner.runScript(xxx),
            runner.runScript(new InputStreamReader(new FileInputStream(file), "utf-8"));

        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    /**
     * @param filePath 只可使用绝对(相对)路径, 不可使用 classpath(network) 路径
     */
    public File exportMySql(String filePath) {
        Exporter.MYSQL.export(new DBConfigure(username, password, dbName, filePath));
        return new File(filePath);
    }

    /**
     * @param file 当前方法总是确保 file 已存在, 若不存在则创建
     */
    public File exportMySql(File file) {
        String path = file.getAbsolutePath();
        FileUtil.createFileParentDir(path);
        return exportMySql(path);
    }

    public enum SqlSeparator {
        MYSQL(";");
        public final String SEPARATOR;

        SqlSeparator(String separator) {
            this.SEPARATOR = separator;
        }
    }


}
