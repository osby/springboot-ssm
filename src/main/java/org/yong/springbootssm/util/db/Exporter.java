package org.yong.springbootssm.util.db;

import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.util.List;

public abstract class Exporter {

    public static final Exporter MYSQL = new Exporter() {
        @Override
        public void export(DBConfigure c) {
            try {
                Process process = Runtime.getRuntime().exec(c.getExportCommand());
                super.validProcessStatus(process);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    };

    private void validProcessStatus(Process process) throws Exception {
        if (0 == process.waitFor())
            return;

        try (InputStream in = process.getErrorStream()) {
            List<String> errs = IOUtils.readLines(in);
            StringBuilder buf = new StringBuilder();
            for (String err : errs)
                buf.append(err).append("\n");
            throw new RuntimeException(buf.toString());
        }
    }

    /**
     * 导出数据
     *
     * @param c 配置
     */
    public abstract void export(DBConfigure c);

}