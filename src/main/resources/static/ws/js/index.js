/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: jinlo
 * Date: 2018-03-18
 * Time: 11:17
 */
$(function () {

    if (!("WebSocket" in window)) {
        alert("Can not support WebSocket");
        return;
    }

    var c = new WebSocket("wss://localhost:8443/ws/abc");
    c.onopen = function (event) {
        console.log(event);
    };

    c.onclose = function (p1) {
        console.log(arguments);
    };

    c.onmessage = function (p1) {
        console.log(arguments);
    };
});