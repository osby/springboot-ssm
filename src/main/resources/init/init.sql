DROP DATABASE IF EXISTS `springboot-ssm`;
CREATE DATABASE IF NOT EXISTS `springboot-ssm` DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

USE `springboot-ssm`;
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ssm_user
-- ----------------------------
DROP TABLE IF EXISTS `ssm_user`;
CREATE TABLE `ssm_user` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;