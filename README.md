# springboot-ssm
spring boot1.7.5, mybatis3, JDK1.7

  - keystore.jks 说明  
    - password  
    123456
    - 其它
    ```
    密钥库类型: JKS
    密钥库提供方: SUN
    
    您的密钥库包含 1 个条目
    
    别名: icesslkey
    创建日期: 2018-3-18
    条目类型: PrivateKeyEntry
    证书链长度: 1
    证书[1]:
    所有者: CN=hyong, OU=hyong, O=hyong, L=cd, ST=sc, C=zh
    发布者: CN=hyong, OU=hyong, O=hyong, L=cd, ST=sc, C=zh
    序列号: 2711d229
    有效期开始日期: Sun Mar 18 12:08:11 CST 2018, 截止日期: Wed Mar 15 12:08:11 CST 2028
    证书指纹:
             MD5: 23:62:ED:3B:99:16:2F:E7:D9:38:AF:D7:9F:F8:A1:AD
             SHA1: 75:9B:C8:57:C5:A4:99:06:8C:46:20:F0:06:D7:6E:21:EC:D2:03:DF
             SHA256: E2:F1:48:D1:5F:67:BE:74:2B:FF:00:DA:A3:73:3E:3D:14:04:3C:2B:9B:BD:B7:91:9E:11:24:75:AC:45:63:F3
             签名算法名称: SHA256withRSA
             版本: 3
    
    扩展:
    
    #1: ObjectId: 2.5.29.14 Criticality=false
    SubjectKeyIdentifier [
    KeyIdentifier [
    0000: B4 DB C2 BE 49 65 BA 08   72 B1 49 FE 4F DB 64 06  ....Ie..r.I.O.d.
    0010: DF 5A 5A 83                                        .ZZ.
    ]
    ]
    
    
    
    *******************************************
    *******************************************
    
    ```
    